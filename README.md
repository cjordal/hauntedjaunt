# John Lemon's Haunted Jaunt

Help John Lemon escape the terrors of a haunted mansion!

By Cameron Jordal and Kristine Stecker.

## Our Work

### Dot Product

Enemies will turn towards you, sensing your presence, when you get too close to them.

### Linear Interpolation

Similarly to the dot product, your view will become increasingly overexposed the closer you get to enemies that sense you.

### Particle Effect

Orange fumes rise out of the floorboards. 

### Contributions

Basic project and environment developed by Kristine Stecker. Additonal features developed by Cameron Jordal.
