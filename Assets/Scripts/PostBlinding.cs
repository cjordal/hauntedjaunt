﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering.PostProcessing;

public class PostBlinding : MonoBehaviour
{
    public PostProcessVolume volume;
    public float maxBloom; 
    public Transform player;
    public CapsuleCollider field;

    private Bloom bloom;
    private float minBloom;
    private bool playerInField;
    private float maxProx;
    private Transform enemy;
    private Vector3 angles;

    // Start is called before the first frame update
    void Start()
    {
        volume.profile.TryGetSettings(out bloom);
        minBloom = bloom.intensity.value;
        playerInField = false;
        maxProx = field.radius;
        enemy = transform.parent;
        angles = new Vector3(0.0f, 1.0f, 0.0f);
    }

    void OnTriggerEnter(Collider other)
    {
        if(other.transform == player)
        {
            playerInField = true;
        }
    }

    void OnTriggerExit(Collider other)
    {
        if (other.transform == player)
        {
            playerInField = false;
        }
    }

    // Update is called once per frame
    void Update()
    {
        // alter states if in enemy sense field
        if (playerInField)
        {
            Vector3 direction = player.position - enemy.position;
            // calculate bloom intensity based on proximity to enemy
            float closeness = 1 - direction.magnitude / maxProx;
            bloom.intensity.value = (1 - closeness) * minBloom + closeness * maxBloom;
            // turn enemy towards player
            direction.Normalize();
            float angle = Mathf.Rad2Deg * Mathf.Acos(Vector3.Dot(Vector3.forward, direction));
            angles.y = angle;
            enemy.eulerAngles = angles;
        }
    }
}
